function EV5_2021_04_01_final_v1(p_in,figure_name,figure_number);
% EV Figure 5. The sigV feedback loop increases the dynamic 
% range of the circuit. 

%setting parameter
p=p_in.EV5;
p.bval=p_in.bval;
out_name=[figure_name,figure_number];
out_name_A=[out_name,'A'];
out_name_B=[out_name,'B'];


%Setting up figure
fig_para=fig_para_pres_func_v1;
ha=figure_special_func_pres(fig_para);

%Plotting Figure EV5 A Model rewire
axes(ha(1));
EV_5A_2021_03_29_final_v1(p,out_name_A,fig_para);


% Plotting Figure B and raw data of figure B
axes(ha(2));
EV_5B_2021_03_29_final_v1(p,out_name_B,fig_para)

%Saving Figure
print(gcf,'-painters',[p.Figure,out_name],'-depsc2');
exportgraphics(gcf,[p.Figure,out_name,'.pdf'],'ContentType','vector');

