function Figure_5C_2021_04_01_final_v4(p,out_name,fig_para);
% Function to plot Figure 5C

%Loading Data
[mutant_name_out,x_value,frac_use_m,frac_use_s]=loading_excel_file(p,out_name);


%plotting
bar(x_value,frac_use_m,'Facecolor','r');
hold on;
errorbar(x_value,frac_use_m,frac_use_s,'.k','CapSize',2);
axis([0.5 4.5 0 1.1]);
beauty_func_v1(fig_para);
mutant_name_out_use=cellfun(@(a) ['2x',a],mutant_name_out,'UniformOutput',false);
set(gca,'XTick',x_value,'XTicklabel',mutant_name_out_use);
ylabel('Fraction of Activated Cells (au)');
hline(frac_use_m(1));
set(gca,'XColor','k','YColor','k');
set(gcf, 'Color', 'w');
title('Experiment');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [mutant_name_out,x_value,frac_use_m,frac_use_s]=loading_excel_file(p,out_name);
in_path=p.Data_Excel;

if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    %converting number to string for condition
    [~,mutant_name_out_pre]=xlsread([in_path,out_name],sheet_names{1});
    mutant_name_out=mutant_name_out_pre(2:end);
    %Loading data
    x_value=xlsread([in_path,out_name],sheet_names{2}); 
    frac_use_m=xlsread([in_path,out_name],sheet_names{3}); 
    frac_use_s=xlsread([in_path,out_name],sheet_names{4}); 
else
    disp('No Excel files. Please generate first');
    return;
end
end

