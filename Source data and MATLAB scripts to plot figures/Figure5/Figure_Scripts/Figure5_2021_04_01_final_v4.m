function Figure5_2021_04_01_final_v4(p_in,out_addition)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figure 5. Phenotypic variability is tuned by additional copies of 
%components of the �V circuit. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Setting Parameters
p=p_in.Figure5;
p.bval=p_in.bval;
out_name_B=[out_addition,'Figure5B'];
out_name_C=[out_addition,'Figure5C'];
out_name_fig5=[out_addition,'Figure5'];


%Setting Figure
fig_para=fig_para_pub_structure;
[ha,~]=figure_special_pub_func_2019_10_30_v1(fig_para);

%Figure 5B model genetic pertubation
axes(ha(2));
Figure_5B_2021_04_01_final_v4(p,out_name_B,fig_para);

%Figure 5C Experiment genetic pertubation
axes(ha(3));
Figure_5C_2021_04_01_final_v4(p,out_name_C,fig_para);


%saving partII Memory Experiment
print(gcf,'-painters','-depsc2',[p.Figure,out_name_fig5]);
exportgraphics(gcf,[p.Figure,out_name_fig5,'.pdf'],'ContentType','vector');


