function [p]=initiate_SigV_figure_data_final_2021_03_25_v1(rootdir)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to initiate the path with all the data of the sigV paper
% Input: rootdir; parent folder with all the data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Main figure folder names
Figure_main_text={'Figure1','Figure2','Figure3','Figure4',...
    'Figure5','Figure6'};
%Sup figure folder names
Figure_sup_text={'EV1','EV2','EV3','EV4','EV5'};
%Folder in each Figure Folder
folder_names={'Data_Excel','Data_Matlab','Figure','Figure_Scripts'};
%Background Value
bval=200;


% Adding Folders to structure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%1. Root FolderMoving_data_into_folders
if rootdir(end)=='\'
    p.rootDir=rootdir;
else
    p.rootDir=[rootdir,'\'];
end

%2. Background value
p.bval=bval;


%3. Adding Main Text Figure to structure
% fmt loops over all figures
% i loops over all folders in figure folder

for fmt=1:length(Figure_main_text)
    p.(Figure_main_text{fmt})=[];
    for i=1:length(folder_names)
        p.(Figure_main_text{fmt}).(folder_names{i})=[p.rootDir,(Figure_main_text{fmt}),'\',folder_names{i},'\'];
        if ~exist(p.(Figure_main_text{fmt}).(folder_names{i}));
            mkdir(p.(Figure_main_text{fmt}).(folder_names{i}));
        end
    end
end

%3. Adding Sup Text Figure to structure
% fst loops over all figures
% i loops over all folders in figure folder
for fst=1:length(Figure_sup_text)
    p.(Figure_sup_text{fst})=[];
    for i=1:length(folder_names)
        p.(Figure_sup_text{fst}).(folder_names{i})=[p.rootDir,Figure_sup_text{fst},'\',folder_names{i},'\'];
        if ~exist(p.(Figure_sup_text{fst}).(folder_names{i}));
            mkdir(p.(Figure_sup_text{fst}).(folder_names{i}));
        end
    end
end
