function Figure_6DE_2021_04_01_final_v4(p,out_name,fig_para,ha)
% Function to plot Figure 6D and 6E 

%Loading Data
[time_x,traces_y]=loading_excel_file(p,out_name);


%plotting
for i=1:length(traces_y)
    %plotting figure
    axes(ha(i));
    plot(time_x{i},traces_y{i});
    axis([0 1500 0 6000]);
    % Make figures nice
    xlabel('Time (Minutes)');
    ylabel('P_{\it{sigV}}\rm\bf - YFP (au)');
    if i==1
        vline(360,'--k');
    else
        vline(720,'--k');
    end
    beauty_func_v1(fig_para);
end


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [time_x,traces]=loading_excel_file(p,out_name)
in_path=p.Data_Excel;
if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    ind=1;
    for i=1:length(sheet_names)
        if mod(i,2)==1
            %Loading time
            time_x{ind}=xlsread([in_path,out_name],sheet_names{i});
        else
            %Loading traces
            traces{ind}=xlsread([in_path,out_name],sheet_names{i}); 
            ind=ind+1;
        end
    end
else
    disp('No Excel files. Please generate first');
    return;
end
end

