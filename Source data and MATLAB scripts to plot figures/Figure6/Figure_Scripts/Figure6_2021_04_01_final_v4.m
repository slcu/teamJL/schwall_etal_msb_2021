function Figure6_2021_04_01_final_v4(p_in,out_addition)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figure 6. The SigV circuit has a memory of previous stress.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%Setting up Figures%%%%%%%%%%%%%%%%%%%%%%%
%Setting Parameters
p=p_in.Figure6;
p.bval=p_in.bval;
out_name_AB=[out_addition,'Figure6AB'];
out_name_C=[out_addition,'Figure6C'];
out_name_DE=[out_addition,'Figure6DE'];
out_name_F=[out_addition,'Figure6F'];
out_name_fig6=[out_addition,'Figure6'];

% %Setting Figure Part1
fig_para=fig_para_pub_structure;
[ha1,hh1]=figure_special_pub_func_2019_10_30_v1(fig_para);
% Setting Figure Part2
[ha2,hh2]=figure_special_pub_func_2019_10_30_v1(fig_para);


%%%%%%%%%%%%%%%%%%%%%%Plotting Figures%%%%%%%%%%%%%%%%%%%%%%%
%Figure 6A&B model short and long memory
Figure_6AB_2021_04_01_final_v4(p,out_name_AB,fig_para,ha1)

%Figure 6C model long memory model
axes(ha1(3));
Figure_6C_2021_04_01_final_v4(p,out_name_C,fig_para);


%Figure 6DE model short memory
Figure_6DE_2021_04_01_final_v4(p,out_name_DE,fig_para,ha2)

% Figure 6F model long memory model
axes(ha2(3));
Figure_6F_2021_04_01_final_v4(p,out_name_F,fig_para);


%%%%%%%%%%%%%%%%%%%%%%%Saving Figures%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %saving partI Memory Model
print(gcf,'-painters','-depsc2',[p.Figure,out_name_fig6,'_part1']);
exportgraphics(gcf,[p.Figure,out_name_fig6,'_part1.pdf'],'ContentType','vector');


% saving partII Memory Experiment
print(gcf,'-painters','-depsc2',[p.Figure,out_name_fig6,'_part2']);
exportgraphics(gcf,[p.Figure,out_name_fig6,'_part2.pdf'],'ContentType','vector');


