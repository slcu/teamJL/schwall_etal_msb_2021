function Figure_1F_2021_04_01_final_v4(p_use,out_name,fig_para)
% Function to plot Figure 1E

%Loading Data
[x_val_main,y_val_main]=load_excel_file(p_use,out_name);


%Plotting turn on
plot(x_val_main,y_val_main,'Linewidth',2);
xline(250,'k--','Linewidth',1,'Alpha',1);

%making figure nice
leg=legend({['0.5 ',char(181),'g/ml'], ['1 ',char(181),'g/ml'],['2 ',char(181),'g/ml'],['4 ',char(181),'g/ml']},'Location','southeast','LineWidth',1,'Edgecolor','k');
xlabel('Time (Minutes)');
ylabel('Cumulative Fraction (au)');
beauty_func_v1(fig_para);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1. Function to Load Excel Data
function [x_val_main,y_val_main]=load_excel_file(p,out_name)
ind=1;
in_path=p.Data_Excel;
if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    for i=1:length(sheet_names);
        if mod(i,2)~=0
            %case of x values
            x_val{ind}=xlsread([in_path,out_name],sheet_names{i});
        else
            %case of y values
            y_val{ind}=xlsread([in_path,out_name],sheet_names{i});
            ind=ind+1;
        end
    end
    %making output matrix
    x_val_main=cell2mat(x_val);
    y_val_main=cell2mat(y_val);;
else
    disp('No Excel files. Please generate first');
    return;
end
end