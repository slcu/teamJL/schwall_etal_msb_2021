function Figure1_2021_04_01_final_v4(p_in,out_addition)
%Figure 1. SigV is activated heterogeneously in response to lysozyme stress.  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%Setting up Figures%%%%%%%%%%%%%%%%%%%%%%%
%Setting Parameters
out_name_fig1=[out_addition,'Figure1'];
out_name_D=[out_addition,'Figure1D'];
out_name_E=[out_addition,'Figure1E'];
out_name_F=[out_addition,'Figure1F'];
out_name_G=[out_addition,'Figure1G'];
p=p_in.Figure1;
p.bval=p_in.bval;

% Setting figure for first part
fig_para=fig_para_pub_structure;
[ha1,hh1]=figure_special_pub_func_2019_10_30_v1(fig_para);
% Setting figure for second part
[ha2,hh2]=figure_special_pub_func_2019_10_30_v1(fig_para);

%%%%%%%%%%%%%%%%%%%%%%Plotting Figures%%%%%%%%%%%%%%%%%%%%%%%
% D) Plottin singel cell traces
axes(ha1(1));
Figure_1D_2021_03_29_final_v4(p,out_name_D,fig_para);

% E) Trace variability
axes(ha2(1));
Figure_1E_2021_04_01_final_v4(p,out_name_E,fig_para);

% F) Turn on 
axes(ha2(2));
Figure_1F_2021_04_01_final_v4(p,out_name_F,fig_para);

% G) Fold Change
axes(ha2(3));
Figure_1G_2021_04_01_final_v4(p,out_name_G,fig_para)

%%%%%%%%%%%%%%%%%%%%%%%Saving Figures%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Saving figure for first part
print(hh1,'-painters','-depsc2',[p.Figure,out_name_fig1,'_part1']);
exportgraphics(hh1,[p.Figure,out_name_fig1,'_part1.pdf'],'ContentType','vector');

%Saving figure for second part
print(hh2,'-painters','-depsc2',[p.Figure,out_name_fig1,'_part2']);
exportgraphics(hh2,[p.Figure,out_name_fig1,'_part2.pdf'],'ContentType','vector');



