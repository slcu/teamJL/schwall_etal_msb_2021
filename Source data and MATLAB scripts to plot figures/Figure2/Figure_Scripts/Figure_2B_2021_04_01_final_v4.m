function Figure_2B_2021_04_01_final_v4(p,out_name,fig_para)
%Function to plot Figure 2B

%Loading Data
[time,num_channels_prime,num_channels_no_prime,prime_mean,no_prime_mean]=loading_excel_files(p,out_name);


%plotting mean
plot(time,prime_mean,'--r','Linewidth',1);
hold on;
%Plotting all data prime
plot(time,num_channels_prime,'r')
hold on;
%Plotting mean no prime
plot(time,no_prime_mean,'--b','Linewidth',1);
%Plotting all data no prime
hold on;
plot(time,num_channels_no_prime,'b')
hold on;
a=axis;
axis([0 280 0 a(4)]);

%Making Figure Nice
xlabel('Time (Min)');
ylabel('Fraction of Surviving Cells (au)');
h=findobj(gca,'Type','line');
legend([h(1),h(3),h(4),h(8)],{'No Priming','Mean No Priming','Priming','Mean Priming'},'FontSize',fig_para.FontSize-2,...
'Edgecolor','k');
box on;
beauty_func_v1(fig_para);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%1. Function to Load Excel Data
function [time,num_channels_prime,num_channels_no_prime,prime_mean, no_prime_mean]=loading_excel_files(p,out_name)
in_path=p.Data_Excel;
if exist([in_path,out_name,'.xlsx']);
    [~,sheet_names]=xlsfinfo([in_path,out_name]);
    %Read no prime mean
    data=xlsread([in_path,out_name],sheet_names{1});
    time=data(:,1);
    no_prime_mean=data(:,2);
    
    %Read prime mean
    data=xlsread([in_path,out_name],sheet_names{2});
    prime_mean=data(:,2);
    
    %Read no prime
    data=xlsread([in_path,out_name],sheet_names{3});
    num_channels_no_prime=data(:,2:end);

    %Read no prime
    data=xlsread([in_path,out_name],sheet_names{4});
    num_channels_prime=data(:,2:end);  
else
    disp('No Excel files. Please generate first');
    return;
end
end




