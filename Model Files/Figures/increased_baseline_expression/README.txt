This file contains all of the figures from the experimental simulations where the baseline expression (v0) of either component of the system is increased.

(I also needed to create some file in this folder to ensure it got uploaded by got, and figured this would be better than a .gitignore file)
