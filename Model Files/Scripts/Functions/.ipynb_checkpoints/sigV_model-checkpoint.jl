### Base Model ###

# Useful for the continious model.
function f(du,u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L,η = p
    du[1] = v0 + (v*σ^n)/(σ^n+K^n) - kB*σ*A + kD*σA + L*kC*σA - deg*σ
    du[2] = v0 + (v*σ^n)/(σ^n+K^n) - kB*σ*A + kD*σA - deg*A
    du[3] = kB*σ*A - kD*σA - L*kC*σA - deg*σA
end
function g(du,u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L,η = p
    du[1,1] = η*sqrt(v0 + (v*σ^n)/(σ^n+K^n))
    du[1,2] = -η*sqrt(deg*σ)
    du[1,3] = 0
    du[1,4] = 0
    du[1,5] = -η*sqrt(kB*σ*A)
    du[1,6] = η*sqrt(kD*σA)
    du[1,7] = η*sqrt(L*kC*σA)
    du[2,1] = η*sqrt(v0 + (v*σ^n)/(σ^n+K^n))
    du[2,2] = 0
    du[2,3] = -η*sqrt(deg*A)
    du[2,4] = 0
    du[2,5] = -η*sqrt(kB*σ*A)
    du[2,6] = η*sqrt(kD*σA)
    du[2,7] = 0
    du[3,1] = 0
    du[3,2] = 0
    du[3,3] = 0
    du[3,4] = -η*sqrt(deg*σA)
    du[3,5] = η*sqrt(kB*σ*A)    
    du[3,6] = -η*sqrt(kD*σA)
    du[3,7] = -η*sqrt(L*kC*σA)     
end

# Useful for the discrete model
function rate1(u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L = p
    return v0 + (v*σ^n)/(σ^n+K^n)
end
function affect1!(integrator)
  integrator.u[1] += 1
  integrator.u[2] += 1
end
jump1 = ConstantRateJump(rate1,affect1!)
function rate2(u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L = p
    return deg*σ
end
function affect2!(integrator)
  integrator.u[1] -= 1
end
jump2 = ConstantRateJump(rate2,affect2!)
function rate3(u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L = p
    return deg*A
end
function affect3!(integrator)
  integrator.u[2] -= 1
end
jump3 = ConstantRateJump(rate3,affect3!)
function rate4(u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L = p
    return deg*σA
end
function affect4!(integrator)
  integrator.u[3] -= 1
end
jump4 = ConstantRateJump(rate4,affect4!)
function rate5(u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L = p
    return kB*σ*A
end
function affect5!(integrator)
  integrator.u[1] -= 1
  integrator.u[2] -= 1
  integrator.u[3] += 1
end
jump5 = ConstantRateJump(rate5,affect5!)
function rate6(u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L = p
    return kD*σA
end
function affect6!(integrator)
  integrator.u[3] -= 1
  integrator.u[1] += 1
  integrator.u[2] += 1
end
jump6 = ConstantRateJump(rate6,affect6!)
function rate7(u,p,t)
    σ,A,σA = u
    v0,v,K,n,kD,kB,kC,deg,L = p
    return L*kC*σA
end
function affect7!(integrator)
  integrator.u[3] -= 1
  integrator.u[1] += 1
end
jump7 = ConstantRateJump(rate7,affect7!);

jumps_base = [jump1, jump2, jump3, jump4, jump5, jump6, jump7]

### Inducible Model ###

# Useful for the continious model.
function f_inducible(du,u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA,η = p
  du[1] = v0 + (v*σ^n)/(σ^n+K^n) - kB*σ*A + kD*σA + L*kC*σA - deg*σ + oS
  du[2] = v0 + (v*σ^n)/(σ^n+K^n) - kB*σ*A + kD*σA - deg*A + oA
  du[3] = kB*σ*A - kD*σA - L*kC*σA - deg*σA
end
function g_inducible(du,u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA,η = p
  du[1,1] = η*sqrt(v0 + (v*σ^n)/(σ^n+K^n))
  du[1,2] = -η*sqrt(deg*σ)
  du[1,3] = 0
  du[1,4] = 0
  du[1,5] = -η*sqrt(kB*σ*A)
  du[1,6] = η*sqrt(kD*σA)
  du[1,7] = η*sqrt(L*kC*σA)
  du[1,8] = η*sqrt(oS)
  du[1,9] = 0
  du[2,1] = η*sqrt(v0 + (v*σ^n)/(σ^n+K^n))
  du[2,2] = 0
  du[2,3] = -η*sqrt(deg*A)
  du[2,4] = 0
  du[2,5] = -η*sqrt(kB*σ*A)
  du[2,6] = η*sqrt(kD*σA)
  du[2,7] = 0
  du[2,8] = 0
  du[2,9] = η*sqrt(oA)
  du[3,1] = 0
  du[3,2] = 0
  du[3,3] = 0
  du[3,4] = -η*sqrt(deg*σA)
  du[3,5] = η*sqrt(kB*σ*A)    
  du[3,6] = -η*sqrt(kD*σA)
  du[3,7] = -η*sqrt(L*kC*σA) 
  du[3,8] = 0
  du[3,9] = 0  
end

# Useful for the discrete model
function rate1_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return v0 + (v*σ^n)/(σ^n+K^n)
end
function affect1_inducible!(integrator)
  integrator.u[1] += 1
  integrator.u[2] += 1
end
jump1_inducible = ConstantRateJump(rate1_inducible,affect1_inducible!)
function rate2_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return deg*σ
end
function affect2_inducible!(integrator)
  integrator.u[1] -= 1
end
jump2_inducible = ConstantRateJump(rate2_inducible,affect2_inducible!)
function rate3_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return deg*A
end
function affect3_inducible!(integrator)
  integrator.u[2] -= 1
end
jump3_inducible = ConstantRateJump(rate3_inducible,affect3_inducible!)
function rate4_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return deg*σA
end
function affect4_inducible!(integrator)
  integrator.u[3] -= 1
end
jump4_inducible = ConstantRateJump(rate4_inducible,affect4_inducible!)
function rate5_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return kB*σ*A
end
function affect5_inducible!(integrator)
  integrator.u[1] -= 1
  integrator.u[2] -= 1
  integrator.u[3] += 1
end
jump5_inducible = ConstantRateJump(rate5_inducible,affect5_inducible!)
function rate6_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return kD*σA
end
function affect6_inducible!(integrator)
  integrator.u[3] -= 1
  integrator.u[1] += 1
  integrator.u[2] += 1
end
jump6_inducible = ConstantRateJump(rate6_inducible,affect6_inducible!)
function rate7_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return L*kC*σA
end
function affect7_inducible!(integrator)
  integrator.u[3] -= 1
  integrator.u[1] += 1
end
jump7_inducible = ConstantRateJump(rate7_inducible,affect7_inducible!);
function rate8_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return oS
end
function affect8_inducible!(integrator)
  integrator.u[1] += 1
end
jump8_inducible = ConstantRateJump(rate8_inducible,affect8_inducible!)
function rate9_inducible(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,oS,oA = p
  return oA
end
function affect9_inducible!(integrator)
  integrator.u[2] += 1
end
jump9_inducible = ConstantRateJump(rate9_inducible,affect9_inducible!)


jumps_inducible = [jump1_inducible, jump2_inducible, jump3_inducible, jump4_inducible, jump5_inducible, jump6_inducible, jump7_inducible, jump8_inducible, jump9_inducible]



### Double Operon Model ###

# Useful for the continious model.
function f_2operon(du,u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV,η = p
  du[1] = v0 + (v*σ^n)/(σ^n+K^n) - kB*σ*A + kD*σA + L*kC*σA - deg*σ + sV*(v0 + (v*σ^n)/(σ^n+K^n))
  du[2] = v0 + (v*σ^n)/(σ^n+K^n) - kB*σ*A + kD*σA - deg*A           + rV*(v0 + (v*σ^n)/(σ^n+K^n))
  du[3] = kB*σ*A - kD*σA - L*kC*σA - deg*σA
end
function g_2operon(du,u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV,η = p
  du[1,1] = η*sqrt(v0 + (v*σ^n)/(σ^n+K^n))
  du[1,2] = -η*sqrt(deg*σ)
  du[1,3] = 0
  du[1,4] = 0
  du[1,5] = -η*sqrt(kB*σ*A)
  du[1,6] = η*sqrt(kD*σA)
  du[1,7] = η*sqrt(L*kC*σA)
  du[1,8] = η*sqrt(sV*(v0 + (v*σ^n)/(σ^n+K^n)))
  du[1,9] = 0.
  du[2,1] = η*sqrt(v0 + (v*σ^n)/(σ^n+K^n))
  du[2,2] = 0
  du[2,3] = -η*sqrt(deg*A)
  du[2,4] = 0
  du[2,5] = -η*sqrt(kB*σ*A)
  du[2,6] = η*sqrt(kD*σA)
  du[2,7] = 0
  du[2,8] = 0.
  du[2,9] = η*sqrt(rV*(v0 + (v*σ^n)/(σ^n+K^n)))
  du[3,1] = 0
  du[3,2] = 0
  du[3,3] = 0
  du[3,4] = -η*sqrt(deg*σA)
  du[3,5] = η*sqrt(kB*σ*A)    
  du[3,6] = -η*sqrt(kD*σA)
  du[3,7] = -η*sqrt(L*kC*σA)     
end

# Useful for the discrete model
function rate1_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return v0 + (v*σ^n)/(σ^n+K^n)
end
function affect1_2operon!(integrator)
  integrator.u[1] += 1
  integrator.u[2] += 1
end
jump1_2operon = ConstantRateJump(rate1,affect1_2operon!)
function rate2_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return deg*σ
end
function affect2_2operon!(integrator)
  integrator.u[1] -= 1
end
jump2_2operon = ConstantRateJump(rate2_2operon,affect2_2operon!)
function rate3_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return deg*A
end
function affect3_2operon!(integrator)
  integrator.u[2] -= 1
end
jump3_2operon = ConstantRateJump(rate3_2operon,affect3_2operon!)
function rate4_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return deg*σA
end
function affect4_2operon!(integrator)
  integrator.u[3] -= 1
end
jump4_2operon = ConstantRateJump(rate4_2operon,affect4_2operon!)
function rate5_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return kB*σ*A
end
function affect5_2operon!(integrator)
  integrator.u[1] -= 1
  integrator.u[2] -= 1
  integrator.u[3] += 1
end
jump5_2operon = ConstantRateJump(rate5_2operon,affect5_2operon!)
function rate6_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return kD*σA
end
function affect6_2operon!(integrator)
  integrator.u[3] -= 1
  integrator.u[1] += 1
  integrator.u[2] += 1
end
jump6_2operon = ConstantRateJump(rate6_2operon,affect6_2operon!)
function rate7_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return L*kC*σA
end
function affect7_2operon!(integrator)
  integrator.u[3] -= 1
  integrator.u[1] += 1
end
jump7_2operon = ConstantRateJump(rate7_2operon,affect7_2operon!);
function rate8_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return sV*(v0 + (v*σ^n)/(σ^n+K^n))
end
function affect8_2operon!(integrator)
  integrator.u[1] += 1
end
jump8_2operon = ConstantRateJump(rate8_2operon,affect8_2operon!)
function rate9_2operon(u,p,t)
  σ,A,σA = u
  v0,v,K,n,kD,kB,kC,deg,L,sV,rV = p
  return rV*(v0 + (v*σ^n)/(σ^n+K^n))
end
function affect9_2operon!(integrator)
  integrator.u[2] += 1
end
jump9_2operon = ConstantRateJump(rate9_2operon,affect9_2operon!)

jumps_2operon = [jump1_2operon, jump2_2operon, jump3_2operon, jump4_2operon, jump5_2operon, jump6_2operon, jump7_2operon, jump8_2operon, jump9_2operon]