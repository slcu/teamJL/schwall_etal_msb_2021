This folder contains the data from the simulations where the additional (induced) operon contains only a SigV gene.

(I also needed to create some file in this folder to ensure it got uploaded by got, and figured this would be better than a .gitignore file)
