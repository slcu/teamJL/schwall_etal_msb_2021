This file contains all of the data from the simulations where an additional operon (which is induced, instead of activated by SigV) is added to the model.

(I also needed to create some file in this folder to ensure it got uploaded by got, and figured this would be better than a .gitignore file)
