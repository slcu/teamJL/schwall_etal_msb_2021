This file contains all of the data from the simulations where the effect of various lengths of the memory holiday (time between stress removal and reactivation) is investigated.

(I also needed to create some file in this folder to ensure it got uploaded by got, and figured this would be better than a .gitignore file)
