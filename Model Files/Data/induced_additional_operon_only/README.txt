This file contains all of the data from the model investigation when the native SigV operon is knocked out, and replaced by one which is instead induced.

(I also needed to create some file in this folder to ensure it got uploaded by got, and figured this would be better than a .gitignore file)
